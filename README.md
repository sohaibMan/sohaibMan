<img width=100% title="Coder Abhi." alt="CoderAbhi.0713" src="https://capsule-render.vercel.app/api?type=waving&color=gradient&customColorList=6,11,20&height=150&section=header&text=Just%20Code%20It🔰&fontSize=40&fontColor=fff&animation=twinkling&fontAlignY=32"/>

### Hi 👋, I am SOHAIB  
<p>
<a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=24&duration=4000&pause=1000&color=F70000&background=FFFFFF00&width=700&height=51&lines=Software+Enginner;Full+stack+developer;" alt="sohaib manah skills " /></a>
</p>

- 💞️ I love what I do, and I do what I love

- 💬 I am looking for open-source projects to contribute in

- 💻 I run Linux and I love open-source. I don't use Arch btw, I use Nano because I don't remember to exit Vim, Sorry Vscode








### Here is my portfolio 
```link
https://sohaibmanah.netlify.app/
```





<h2>:fire: My Stats :</h2>
<br>

<p align="center">
  <img src="http://github-readme-streak-stats.herokuapp.com?user=sohaibMan&theme=dark&hide_border=true&border_radius=8" />
</p>
<!-- [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=sohaibMan)](https://github.com/anuraghazra/github-readme-stats)
 -->
<p align="center">&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=sohaibMan&show_icons=true&locale=en" alt="sohaibMan" />
 </p>
<p align="center">
<img align="center" src="https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=sohaibMan&theme=github_dark" alt="sohaib manah" />
</p>


<h2 align="center"> <img src="https://media.giphy.com/media/iY8CRBdQXODJSCERIr/giphy.gif" width="35px">&nbsp; Views and Followers :eyes:</h2>

<p align="center">
    
<a href="https://github.com/sohaibMan/github-profile-views-counter">
    <img src="https://komarev.com/ghpvc/?username=sohaibMan">
</a>
    <a href="https://github.com/sohaibMan?tab=followers">
        <img src="https://img.shields.io/github/followers/sohaibMan?label=Followers&style=social" alt="GitHub Badge"/>
    </a>
</p>
 
